/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2015 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "BondSwapGPU.cuh"
#include "TextureTools.h"

/*! \file BondSwapGPU.cu
    \brief CUDA kernels for BondSwapGPU
*/
scalar4_tex_t pdata_pos_tex;
//! Kernel that finds potential swap partners based on tag, chain pos, and distance criteria
/*! \param d_pos position of particles
    \param d_tag tag of particles
    \param d_Rtag reverse lookup tags
    \param d_chain_pos chain_position labels
    \param box simulation box
    \param d_n_neigh number of neighbors
    \param d_nlist neighborlist
    \param d_head_list head list for neighbors
    \param N number of particles
    \param goodNeighs array of information used to communicate potential swap sets back to the CPU code
    \param bondN maximum length of any particle's entry in the auxiliary bond_table structure
    \param bond_table auxiliary table. A flat reflection of m_bond_table on the gpu
    \param minDist minimum distance between particles to have their bonds swapped
    This kernel executes one thread per particle and tries to find one potential swap set for each.
*/
extern "C" __global__
void gpu_find_bond_swaps_one_kernel(Scalar4 *d_pos,
                                 unsigned int *d_tag,
                                 unsigned int *d_Rtag,
                                 unsigned int *d_chain_pos,
                                 const BoxDim box,
                                 unsigned int *d_n_neigh,
                                 unsigned int *d_nlist,
                                 unsigned int *d_head_list,
                                 unsigned int N,
                                 int *goodNeighs,
                                 unsigned int bondN,
                                 int *bond_table,
                                 Scalar minDist)
    {

    //bond_table is a flat version of m_bond_table
    //if a set is found goodNeighs[iTag] should be:
    // gN[iTag]  =jTag
    // gN[iTag+1]=swap_tag_i
    // gN[iTag+2]=swap_tag_j
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int iTag,jTag,chainPosI,chainPosJ;
    if (idx < N)
        {
        //texFetchScalar and d_pos[idx] seem similar in performance here?
        Scalar4 postypei = texFetchScalar4(d_pos, pdata_pos_tex, idx);
        Scalar3 pi = make_scalar3(postypei.x, postypei.y, postypei.z);

        iTag = d_tag[idx];
        //initialize the table to negative values, indicating a swap set hasn't been found yet
        goodNeighs[3*iTag]   = -1;
        goodNeighs[3*iTag+1] = -1;
        goodNeighs[3*iTag+2] = -1;

        unsigned int b1idx = bondN*iTag;
        chainPosI = d_chain_pos[iTag];
        //loop over neighbors,
        const unsigned int myHead = d_head_list[idx];
        const unsigned int nsize = d_n_neigh[idx];
        for (unsigned int k = 0; k < nsize; k=k+1)
            {
            unsigned int j  = d_nlist[myHead+k];
            jTag = d_tag[j];
            chainPosJ = d_chain_pos[jTag];
            if(chainPosI==chainPosJ)
                {
                unsigned int b2idx = bondN*jTag;
                for (unsigned int b1 = b1idx; b1 < b1idx+bondN; b1=b1+3)
                    for (unsigned int b2 = b2idx; b2 < b2idx+bondN; b2=b2+3)
                        if(bond_table[b1+1]==bond_table[b2+1] && bond_table[b1+1]> -1)
                            {//chain_pos of nearby bonded particles are equal

                            unsigned int idx_in = d_Rtag[bond_table[b1]];
                            unsigned int idx_jn = d_Rtag[bond_table[b2]];
                            // don't double the set of swap candidates
                            if(j < idx || j>idx_jn || idx > idx_in) continue;
                            //necessary check for swaps between ends of the same chain
                            if(idx_in == idx_jn || idx_in == j) continue;
                            Scalar4 postypejn = texFetchScalar4(d_pos, pdata_pos_tex, idx_jn);
                            Scalar3 p_jn = make_scalar3(postypejn.x, postypejn.y, postypejn.z);
                            Scalar3 dx = pi-p_jn;
                            dx=box.minImage(dx);
                            Scalar rsq = dot(dx,dx);
                            if (rsq < minDist)
                                {
                                //(pi,p_jn) are close, now check (pj,p_in)
                                    Scalar4 postypej = texFetchScalar4(d_pos, pdata_pos_tex, j);
                                    Scalar3 pj = make_scalar3(postypej.x, postypej.y, postypej.z);
                                    Scalar4 postypein = texFetchScalar4(d_pos, pdata_pos_tex, idx_in);
                                    Scalar3 p_in = make_scalar3(postypein.x, postypein.y, postypein.z);
                                    Scalar3 dx2 = pj-p_in;
                                    dx2 = box.minImage(dx2);
                                    Scalar rsq2 = dot(dx2,dx2);
                                    if(rsq2 < minDist)
                                        {
                                        //found a potential swap set!
                                        goodNeighs[3*iTag]   = jTag;
                                        goodNeighs[3*iTag+1] = bond_table[b1];
                                        goodNeighs[3*iTag+2] = bond_table[b2];
                                        break;
                                        };

                                };

                            };
                }
            }
        }
    }

/*! \param d_pos position of particles
    \param d_tag tag of particles
    \param d_Rtag reverse lookup tags
    \param d_chain_pos chain_position labels
    \param box simulation box
    \param d_n_neigh number of neighbors
    \param d_nlist neighborlist
    \param d_head_list head list for neighbors
    \param N number of particles
    \param goodNeighs array of information used to communicate potential swap sets back to the CPU code
    \param bondN maximum length of any particle's entry in the auxiliary bond_table structure
    \param bond_table auxiliary table. A flat reflection of m_bond_table for the gpu
    \param minDist minimum distance between particles to have their bonds swapped
    \param block_size block size to use. Controlled by the class' autotuner.
    This is just a driver for gpu_find_bond_swaps_one_kernel()
*/
cudaError_t gpu_find_bond_swap_candidate_one(Scalar4 *d_pos,
                                 unsigned int *d_tag,
                                 unsigned int *d_Rtag,
                                 unsigned int *d_chain_pos,
                                 const BoxDim box,
                                 unsigned int *d_n_neigh,
                                 unsigned int *d_nlist,
                                 unsigned int *d_head_list,
                                 unsigned int N,
                                 int *goodNeighs,
                                 unsigned int bondN,
                                 int *b_table,
                                 Scalar minDist,
                                 unsigned int block_size)
    {
    // setup the grid to run the kernel
    //unsigned int block_size = 128;
    int n_blocks =N/block_size + 1;
    // run the kernel
    static const size_t size = 3*N*sizeof(int);
    static const size_t sizeB = bondN*N*sizeof(int);
    //int* d_goodNeighs = (int*)malloc(size);
    //int* d_b_table = (int*)malloc(sizeB);
    int *d_goodNeighs, *d_b_table;
    cudaMalloc(&d_goodNeighs,size);
    cudaMalloc(&d_b_table,sizeB);

    cudaMemcpy(d_b_table,b_table,sizeB,cudaMemcpyHostToDevice);
    //cudaMemcpy(d_goodNeighs,goodNeighs,size,cudaMemcpyHostToDevice);
    gpu_find_bond_swaps_one_kernel<<< n_blocks, block_size >>>(d_pos,d_tag,d_Rtag,d_chain_pos,box,
                                                               d_n_neigh,d_nlist,d_head_list,N,d_goodNeighs,
                                                               bondN, d_b_table,minDist);
    cudaMemcpy(goodNeighs,d_goodNeighs,size,cudaMemcpyDeviceToHost);
    cudaFree(d_goodNeighs);
    cudaFree(d_b_table);
    // this method always succeds?
    return cudaSuccess;
    }

