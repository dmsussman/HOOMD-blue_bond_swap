/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2015 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifdef ENABLE_CUDA

#include <boost/shared_ptr.hpp>
#include "BondSwap.h"
#include <saruprng.h>
#include "Autotuner.h"

#ifndef __BOND_SWAP_GPU_H__
#define __BOND_SWAP_GPU_H__

//! Class for performing bond swaps according to a Monte Carlo routine on the GPU
/*!
    This class implements a Monte Carlo procedure for swapping bonds between polymers. It is very
    similar to the CPU implementation, mostly just overloading the findPotentialSwaps function.

    The new findPotentialSwaps function replaces the loop in which each particle is checked for a
    potential swap quad (a set iTag-swap_tag_i, jTag-swap_tag_j) that satisfies the inter-particle
    distance and m_chain_position relations) with a GPU kernel call in which each thread
    corresponds to one of the particles. One logical difference in the operation of the CPU and GPU
    versions is the following. In the CPU code m_fraction of the particles are selected to search for
    swap quads, and if a particle is selected then every swap quad involving that particle is found.
    In the GPU code the kernel returns <em> at most </em> one potential swap quad for each particle,
    and then every particle triggers (whether it found a swap quad or not) a saru RNG call to see
    if one should include any swap quad associated with it for swapping.

    One change to the update function is as follows. In order for the GPU kernel to do the work of looking
    for swap candidates it needs to know not only about m_chain_positions (already a GPUarray, and easily
    transfered to the GPU), but also about what particles are bonded to other particles. Currently this is
    done by making a member array, m_b_table, which is simply a flattened version of m_bond_table (it makes
    use of mm_max_bond_N = 3*(max number of bonds any particle has) to keep track of how large to be).
    This m_b_table is passed to the kernel, and is updated when BondSwapGPU is first created and any time
    thereafter that a bond is swapped. This is likely not the best way to implement this, but the current
    performance of the routines seems acceptable.
*/
class BondSwapGPU : public BondSwap
    {
    public:
        //! Constructor
        BondSwapGPU(boost::shared_ptr<SystemDefinition> sysdef,
                boost::shared_ptr<NeighborList> nlist,
                Scalar fraction,
                Scalar distSquared,
                Scalar Tnew,
                int seed,
                int period);

        //! The main routine. Look for and perform bond swaps
        virtual void update(unsigned int timestep);

        //! standard overload of setAutotunerParams
        virtual void setAutotunerParams(bool enable, unsigned int period)
          {
          BondSwap::setAutotunerParams(enable, period);
          m_tuner_swapset->setPeriod(period/10);
          m_tuner_swapset->setEnabled(enable);
          }

    private:
        //! Autotuner for the gpu_find_bond_swap_candidate_one driver
        boost::scoped_ptr<Autotuner> m_tuner_swapset;

        //! Search nearby particles for potential swap proposals (pair and bond distance criteria)
        virtual void findPotentialSwaps(std::vector<std::vector<int> > &quads, int timestep);

        //!copy m_bond_table to m_b_table
        void flattenBondTable();

        //!helper variable for the flattened bond table. Equal to 3*(max number of bonds any particle has)
        unsigned int m_max_bond_N;

        //!flat version of m_bond_table for passing to GPU. m_max_bond_N*(number of particle) is its size
        std::vector<int> m_b_table;

        //!a flag for rebuilding the flat bond table
        bool m_rebuild_bond_table;
    };

//! Export the BondSwapGPU class to python
void export_BondSwapGPU();

#endif // #ifndef __BOND_SWAP_GPU_H__

#endif // ENABLE_CUDA

