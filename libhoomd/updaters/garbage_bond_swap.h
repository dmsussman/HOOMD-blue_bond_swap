        Scalar3 pi1 = make_scalar3(h_pos.data[idx_i1].x,h_pos.data[idx_i1].y,h_pos.data[idx_i1].z);
        Scalar3 pj1 = make_scalar3(h_pos.data[idx_j1].x,h_pos.data[idx_j1].y,h_pos.data[idx_j1].z);
            
        Scalar3 pi2 = make_scalar3(h_pos.data[idx_i2].x,h_pos.data[idx_i2].y,h_pos.data[idx_i2].z);
        Scalar3 pj2 = make_scalar3(h_pos.data[idx_j2].x,h_pos.data[idx_j2].y,h_pos.data[idx_j2].z);

        Scalar3 dx1_original = pi1-pi2;
        dx1_original = box.minImage(dx1_original);
        Scalar3 dx2_original = pj1-pj2;
        dx2_original = box.minImage(dx2_original);
        unsigned int idx_i1, idx_i2,idx_j1,idx_j2;
        idx_i1 = m_pdata->getRTag(pairs[ps][0]);
        idx_j1 = m_pdata->getRTag(pairs[ps][1]);
        idx_i2 = m_pdata->getRTag(swap_tag_i);
        idx_j2 = m_pdata->getRTag(swap_tag_j);


        if(m_nlist->isExcluded(pairs[ps][0],swap_tag_i)) cout << "original exclusion at start." << endl;
        if (swap_tag_i == 0) continue;
        if(bond_i==bond_j) continue;
        
        m_nlist->clearExclusions();
        m_nlist->addExclusionsFromBonds();
        m_nlist->forceUpdate();
        m_nlist->compute(timestep);
        if(m_nlist->isExcluded(pairs[ps][0],swap_tag_i)) cout << "original exclusion intermediate" << endl;
        if(m_nlist->isExcluded(pairs[ps][0],swap_tag_j)) cout << "new exclusion intermediate" << endl;
        //iterate over force computes
        std::vector< boost::shared_ptr<ForceCompute> >::iterator force_compute;
        for (force_compute = m_forces.begin(); force_compute != m_forces.end(); ++force_compute)
            {
            std::vector< std::string > f_log_name;
            f_log_name = (*force_compute)->getProvidedLogQuantities();
            for (unsigned int name = 0; name < f_log_name.size(); ++name)
                {
                unsigned int ftype = forceComputeType(f_log_name[name]);
                switch(ftype)
                    {
                    case 1:     {
                                (*force_compute)->forceCompute(timestep);
                                energy_f +=(*force_compute)->calcEnergySum();
                                break;
                                };



                    case 2:     {
                                (*force_compute)->forceCompute(timestep);
                                energy_f +=(*force_compute)->calcEnergySum();
                                break;
                                };
                    };
                };

            };
        Scalar e_diff = energy_f-energy_i;
        cout << "energy difference = " << energy_f - energy_i << endl;
        Scalar boltzmann_accept = exp(-e_diff/m_Tnew);
        if(saru.f(0,1) > boltzmann_accept)
            {
            //swap 'em back
            h_bonds.data[bond_i].tag[0]=pairs[ps][0];
            h_bonds.data[bond_i].tag[1]=swap_tag_i;
            h_bonds.data[bond_j].tag[0]=pairs[ps][1];
            h_bonds.data[bond_j].tag[1]=swap_tag_j;
            m_nlist->clearExclusions();
            m_nlist->addExclusionsFromBonds();
            m_nlist->forceUpdate();
            m_nlist->compute(timestep);
            };
        if(m_nlist->isExcluded(pairs[ps][0],swap_tag_i)) cout << "original exclusion at end" << endl;
        if(m_nlist->isExcluded(pairs[ps][0],swap_tag_j)) cout << "new exclusion at end" << endl;





//
//old function
//

    // loop over possible swap pairs and compute energetic cost of a switch
    for (unsigned int ps = 0; ps < pairs.size(); ++ps)
    //for (unsigned int ps = 0; ps < 1; ++ps)
        {
        //what bond indices are used in that pair?

        unsigned int swap_tag_i=0;
        unsigned int swap_tag_j=0;
        for (unsigned int a1 = 0; a1 < m_bonded[pairs[ps][0]].size(); ++a1)
            for (unsigned int a2 = 0; a2 < m_bonded[pairs[ps][1]].size(); ++a2)
                if(h_chain_position.data[m_bonded[pairs[ps][0]][a1]] == h_chain_position.data[m_bonded[pairs[ps][1]][a2]])
                    {
                    swap_tag_i = m_bonded[pairs[ps][0]][a1];
                    swap_tag_j = m_bonded[pairs[ps][1]][a2];
                    break;
                    }
        //if swap_tag's were not done correctly, skip the pair
        //CHANGE THIS TO A REAL FAILURE CONDITION



    //    cout<< endl <<  " data for pair: " << pairs[ps][0] << "   " << pairs[ps][1] << "  timestep " << timestep <<  endl;
//        cout << "will attempt to change the " << pairs[ps][0] <<"-" <<swap_tag_i << "  and " << pairs[ps][1] <<"-" <<swap_tag_j << " bonds" << endl;
        changeBondByTag(pairs[ps][0],swap_tag_i,pairs[ps][0],swap_tag_j);
        changeBondByTag(pairs[ps][1],swap_tag_j,pairs[ps][1],swap_tag_i);
        m_nlist->clearExclusions();
        m_nlist->addExclusionsFromBonds();
        m_nlist->forceUpdate();
        m_nlist->compute(timestep);


        Scalar energy_f = getEnergyStep(true,timestep);
        Scalar e_diff = energy_f - energy_i;

        Scalar cost = exp(-e_diff/m_Tnew);
        if(saru.f(0,1) > cost)
            {
            changeBondByTag(pairs[ps][0],swap_tag_j,pairs[ps][0],swap_tag_i);
            changeBondByTag(pairs[ps][1],swap_tag_i,pairs[ps][1],swap_tag_j);
            m_nlist->clearExclusions();
            m_nlist->addExclusionsFromBonds();
            m_nlist->forceUpdate();
            m_nlist->compute(timestep);
            }
        else
            {
            //update m_bonded lists
            for (unsigned int bb = 0; bb < m_bonded[pairs[ps][0]].size(); ++bb)
                if(m_bonded[pairs[ps][0]][bb] == swap_tag_i)
                    m_bonded[pairs[ps][0]][bb] = swap_tag_j;
            for (unsigned int bb = 0; bb < m_bonded[pairs[ps][1]].size(); ++bb)
                if(m_bonded[pairs[ps][1]][bb] == swap_tag_j)
                    m_bonded[pairs[ps][1]][bb] = swap_tag_i;

            unsigned int idx_i1, idx_i2,idx_j1,idx_j2;
            idx_i1 = m_pdata->getRTag(pairs[ps][0]);
            idx_j1 = m_pdata->getRTag(pairs[ps][1]);
            idx_i2 = m_pdata->getRTag(swap_tag_i);
            idx_j2 = m_pdata->getRTag(swap_tag_j);


            cout << "delta_E: " << e_diff << "  cost: " << cost << endl;
            cout << "changed the " << pairs[ps][0] <<"-" <<swap_tag_i << "  and " << pairs[ps][1] <<"-" <<swap_tag_j << " bonds" << endl;

            cout <<"particle i1 : " << h_pos.data[idx_i1].x << "   " << h_pos.data[idx_i1].y << "  " << h_pos.data[idx_i1].z << endl;
            cout <<"particle i2 : " << h_pos.data[idx_i2].x << "   " << h_pos.data[idx_i2].y << "  " << h_pos.data[idx_i2].z << endl;
            cout <<"particle j1 : " << h_pos.data[idx_j1].x << "   " << h_pos.data[idx_j1].y << "  " << h_pos.data[idx_j1].z << endl;
            cout <<"particle j2 : " << h_pos.data[idx_j2].x << "   " << h_pos.data[idx_j2].y << "  " << h_pos.data[idx_j2].z << endl;
            cout <<endl;
            Scalar pairCost = getEnergyPairBond(pairs[ps][0],swap_tag_i,pairs[ps][1],swap_tag_j);

            m_nlist->clearExclusions();
            m_nlist->addExclusionsFromBonds();
            m_nlist->forceUpdate();
            m_nlist->compute(timestep);
            h_mc_stats.data[1] +=1;
            break;
            };




//old m_bonded structure

//.. in constructor
    //make a new data structure listing which particles
    //are bonded to particle with tag [i]
    m_bonded.resize(m_pdata->getNGlobal());
    for (unsigned int ii = 0; ii < size; ++ii)
        {
        //const BondData::members_t& bond = h_bonds.data[ii];
        const BondData::members_t& bond = bonds[ii];
        m_bonded[bond.tag[0]].push_back(bond.tag[1]);
        m_bonded[bond.tag[1]].push_back(bond.tag[0]);
        };


//...in search for good pairs
/*
        for (unsigned int a1 = 0; a1 < m_bonded[pairs[ps][0]].size(); ++a1)
            for (unsigned int a2 = 0; a2 < m_bonded[pairs[ps][1]].size(); ++a2)
                if(h_chain_position.data[m_bonded[pairs[ps][0]][a1]] == h_chain_position.data[m_bonded[pairs[ps][1]][a2]])
                    {
                    swap_tag_i = m_bonded[pairs[ps][0]][a1];
                    swap_tag_j = m_bonded[pairs[ps][1]][a2];
                    break;
                    }
        for (unsigned int ii = 0; ii < bsize; ++ii)
            {
            const BondData::members_t& bond = h_bonds.data[ii];
            if(!found_b1)
                {
                if( bond.tag[0] == swap_tag_i|| bond.tag[1] == swap_tag_i)
                    {
                    bond_i1 = ii;
                    found_b1 = true;
                    };
                };
            if(!found_b2)
                {
                if( bond.tag[0] == swap_tag_j ||  bond.tag[1] == swap_tag_j)
                    {
                    bond_i2 = ii;
                    found_b2 = true;
                    };
                };
            };
*/

//... if a good swap was MC selected
            //update m_bonded lists
/*
            for (unsigned int bb = 0; bb < m_bonded[pairs[ps][0]].size(); ++bb)
                if(m_bonded[pairs[ps][0]][bb] == swap_tag_i)
                    m_bonded[pairs[ps][0]][bb] = swap_tag_j;
            for (unsigned int bb = 0; bb < m_bonded[pairs[ps][1]].size(); ++bb)
                if(m_bonded[pairs[ps][1]][bb] == swap_tag_j)
                    m_bonded[pairs[ps][1]][bb] = swap_tag_i;
*/


//////
//old pairs structure
/////
    //ax this pair structure, replace with GPUVector?
    std::vector<std::vector<unsigned int> > pairs;
    std::vector<unsigned int> pair(2,0);

/////////////////////////
    //First, find potential swap partners,
    //need to also CHECK DISTANCES of bond swap partners: (i,i+1), (j,j+1), (i,j+1), (j, i+1) all less than m_distSquared
    // ALSO need to check that the four atoms are all distinct

    // in light of above, do m_bonded structure first: for each particle figure out what it is bonded to and also note the chainpos of those bonded particles
    for (unsigned int i = 0; i < m_pdata->getN(); i++)
        {
        //consider only a fraction of particles for swapping:
        if(saru.f(0,1) > m_fraction) continue;

        //get tag and position of this particle
        unsigned int iTag = h_tag.data[i];
        Scalar3 pi = make_scalar3(h_pos.data[i].x, h_pos.data[i].y,  h_pos.data[i].z);
        unsigned int chainPosI = h_chain_position.data[iTag];
//cout << "local rank " << local_rank << " particle " <<iTag << endl;


        //loop over all neighbors of this particle
        const unsigned int myHead = h_head_list.data[i];
        const unsigned int size = (unsigned int)h_n_neigh.data[i];
        for (unsigned int k = 0; k < size; ++k)
            {
            //get index and tag of this neighbor
            unsigned int j = h_nlist.data[myHead+k];
            assert(j < m_pdata->getN() + m_pdata->getNGhosts());
            unsigned int jTag = h_tag.data[j];
            unsigned int chainPosJ = h_chain_position.data[jTag];
            if(chainPosI==chainPosJ)
                {
                Scalar3 pj = make_scalar3(h_pos.data[j].x, h_pos.data[j].y,  h_pos.data[j].z);
                Scalar3 dx = pi-pj;
                dx = box.minImage(dx);
                Scalar rsq = dot(dx,dx);
                if(rsq <= m_distSquared)
                    {
                    pair[0]=iTag;
                    pair[1]=jTag;
                    pairs.push_back(pair);
                    };

                };
            };
        };


